<?php
$layoutJsonStr = @file_get_contents(__DIR__ . '/layout.json');
$layoutJson = json_decode($layoutJsonStr, true);

if (isset($layoutTitle) === FALSE) {
    $layoutTitle = 'Výchozí titulek stránky';
    if (array_key_exists('title', $layoutJson) === TRUE) {
        $layoutTitle = $layoutJson['title'];
    }
}
if (isset($layoutKey) === FALSE) {
    $layoutKey = 'key';
    if (array_key_exists('key', $layoutJson) === TRUE) {
        $layoutKey = $layoutJson['key'];
    }
}
if (isset($layoutDescription) === FALSE) {
    $layoutDescription = 'Výchozí description stránky';
    if (array_key_exists('description', $layoutJson) === TRUE) {
        $layoutDescription = $layoutJson['description'];
    }
}
if (isset($layoutRobots) === FALSE) {
    $layoutRobots = 'noindex, follow';
    if (array_key_exists('robots', $layoutJson) === TRUE) {
        $layoutRobots = $layoutJson['robots'];
    }
}
if (isset($layoutCanonical) === FALSE) {
    $layoutCanonical = '';
}

if (isset($layoutVersionUrl) === FALSE) {
    $layoutVersionUrl = '';
    if (array_key_exists('version', $layoutJson) === TRUE && array_key_exists('url', $layoutJson['version']) === TRUE) {
        $layoutVersionUrl = $layoutJson['version']['url'];
    }
}

if (isset($layoutVersionCurrent) === FALSE) {
    $layoutVersionCurrent = '';
    if (array_key_exists('version', $layoutJson) === TRUE && array_key_exists('current', $layoutJson['version']) === TRUE) {
        $layoutVersionCurrent = $layoutJson['version']['current'];
    }
}

$layoutNewVersionInfo = '';
if (empty($layoutVersionUrl) === FALSE && empty($layoutVersionCurrent) === FALSE) {
    $versionJsonStr = @file_get_contents($layoutVersionUrl);
    $versionJson = json_decode($versionJsonStr, true);
    if (is_null($versionJson) === FALSE) {
        $newVersion = 0;
        if (array_key_exists('version', $versionJson) === TRUE) {
            $newVersion = $versionJson['version'];
        } 
        if ($layoutVersionCurrent < $newVersion) {
            $newVersionDownload = '';
            if (array_key_exists('downloadUrl', $versionJson) === TRUE) {
                $newVersionDownload = $versionJson['downloadUrl'];
            }
            $layoutNewVersionInfo = '<div class="new-version"><p>';
            $layoutNewVersionInfo = $layoutNewVersionInfo . 'Nainstalovaná verze (' . $layoutVersionCurrent . ') je nižší než aktuální dostupná (' . $newVersion . ').';
            if (empty($newVersionDownload) === FALSE) {
                $layoutNewVersionInfo = $layoutNewVersionInfo . '</p><p><a href="' . $newVersionDownload . '">Stažení nejnovější verze</a>';
            }
            $layoutNewVersionInfo = $layoutNewVersionInfo . '</p></div>';
        }
    }
}

?>
<!DOCTYPE html>
<html xml:lang="cs" lang="cs">
    <head>
        <title><?php echo $layoutTitle; ?></title>
        <link rel="shortcut icon" href="<?php echo $layoutKey; ?>-favicon.ico" type="image/x-icon"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="<?php echo $layoutRobots; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php
        if (empty($layoutCanonical) === FALSE) {
            echo '<link rel="canonical" href="' . $layoutCanonical . '" />';
        }
        ?>
        
        <meta name="twitter:site" content="@Ch4rlieB" />
        <meta name="twitter:creator" content="@Ch4rlieB" />
        <meta name="twitter:title" content="<?php echo $layoutTitle; ?>" />
        <meta name="twitter:description" content="<?php echo $layoutDescription; ?>" />
        
        <meta name="og:title" content="<?php echo $layoutTitle; ?>" />
        <meta name="og:description" content="<?php echo $layoutDescription; ?>" />
        <meta name="og:type" content="website" />
        <meta name="og:url" content="<?php echo $layoutCanonical; ?>" />
        <meta name="og:image" content="" />

        <style>
            body {
                background-color: #DDD;
            }
            .button {
                background-color: rgb(255, 220, 119);
                border: none;
                color: black;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
            }
            .button:disabled {
                background-color: #dddddd;
                color: gray;
            }
            .input {
                /*margin: 50px;*/
                width: 200px;
                padding: 5px 5px 5px 5px;
                font-size: 16px;
                border: 1px solid #ccc;
                /*height: 34px;*/
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                /*background: url(https://charlieblog.eu/favicon.ico) 96% / 15% no-repeat #eee;*/
            }
            .select {
                width: 212px;
                border-radius: 0;
            }
            .new-version {
                width: 100%;
                background-color: #F77;
                padding: 10px;
                text-align: center;
            }
            .box {
                max-width: 1000px;
                padding: 20px 20px 20px 20px;
                margin-top: 0px;
                margin-bottom: 0px;
                margin-left: auto;
                margin-right: auto;
                border-left: 1px solid #ccc;
                border-right: 1px solid #ccc;
            }
            .header {
                background-color: rgb(255, 220, 119);
            }
            .content {
                background-color: white;
                xpadding: 10px 20px;
            }
            .footer {
                width: 100%;
                background-color: #DDD;
                color: black;
            }
            .footer-version {
                padding-top: 10px; 
                font-size: 8px;
            }

            /*CUSTOM CHECKBOXES*/
            /* Customize the label (the container) */
            .container {
                display: block;
                position: relative;
                padding-left: 32px;
                /*margin-bottom: 12px;*/
                cursor: pointer;
                /*font-size: 22px;*/
                min-height: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .container input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 22px;
                width: 22px;
                background-color: #eee;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked ~ .checkmark {
                background-color: rgb(255, 220, 119);/*#2196F3;*/
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked ~ .checkmark:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
                left: 7px;
                top: 3px;
                width: 5px;
                height: 10px;
                border: solid black;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            /*.center {
                margin-left:auto; 
                margin-right:auto;
            }*/
        </style>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <?php echo $layoutNewVersionInfo; ?>
        <div class="header">
            <div class="box">
            <img src="<?php echo $layoutKey; ?>-icon.png" alt="Update icon" style="width: 35px; float: left; margin-right: 20px"/>
            <h1 style="padding: 0px; margin: 0px;"><?php echo $layoutTitle; ?></h1>
            </div>
        </div>
        <div class="content">
            <div class="box">
