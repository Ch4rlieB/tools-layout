<?php
if (isset($layoutJson) === FALSE) {
    // TODO reread layout.json
    $layoutJson = [];
}
if (isset($layoutAutorName) === FALSE) {
    $layoutAutorName = 'Kája z Norska';
    if (array_key_exists('autor', $layoutJson) === TRUE && array_key_exists('name', $layoutJson['autor']) === TRUE) {
        $layoutAutorName = $layoutJson['autor']['name'];
    }
}
if (isset($layoutAutorUrl) === FALSE) {
    $layoutAutorUrl = 'https://charlieblog.eu/autor';
    if (array_key_exists('autor', $layoutJson) === TRUE && array_key_exists('url', $layoutJson['autor']) === TRUE) {
        $layoutAutorUrl = $layoutJson['autor']['url'];
    }
}
if (isset($layoutArticleUrl) === FALSE) {
    $layoutArticleUrl = '';
    if (array_key_exists('article', $layoutJson) === TRUE && array_key_exists('url', $layoutJson['article']) === TRUE) {
        $layoutArticleUrl = $layoutJson['article']['url'];
    }
}
if (isset($layoutArticleName) === FALSE) {
    $layoutArticleName = '';
    if (array_key_exists('article', $layoutJson) === TRUE && array_key_exists('name', $layoutJson['article']) === TRUE) {
        $layoutArticleName = $layoutJson['article']['name'];
    }
}
?>
<br/>
Autorem je <a href="<?php echo $layoutAutorUrl; ?>"><?php echo $layoutAutorName; ?></a>. 
Pokud máte zájem o <a href="<?php echo $layoutArticleUrl; ?>"><?php echo $layoutArticleName; ?></a>, kontaktujte mě :-)<br/>
</div>    
</body>
</html>